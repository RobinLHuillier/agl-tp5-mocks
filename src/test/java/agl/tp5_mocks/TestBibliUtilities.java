package agl.tp5_mocks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TestBibliUtilities {
	
	private NoticeBibliographique notice1;
	private NoticeBibliographique notice2;
	private NoticeBibliographique notice3;
	private NoticeBibliographique notice4;
	private NoticeBibliographique notice5;
	private NoticeBibliographique notice6;
	private NoticeBibliographique notice7;
	private NoticeBibliographique notice8;
	private ArrayList<NoticeBibliographique> listeNotices;
	private ArrayList<NoticeBibliographique> retour;
	private Bibliothèque biblio;
	
	@Mock
	Clock mockedClock;
	
	@Mock
	InterfaceGlobalBibliographyAccess glob;
	
	@InjectMocks
	BibliUtilities bibUtil = new BibliUtilities();
	
	@BeforeEach
	void setup() {
		notice1 = new NoticeBibliographique("1", "Marche ou crève", "Stephen King");
		notice2 = new NoticeBibliographique("2", "Carrie", "Stephen King");
		notice3 = new NoticeBibliographique("3", "Charlie", "Stephen King");
		notice4 = new NoticeBibliographique("4", "Christine", "Stephen King");
		notice5 = new NoticeBibliographique("5", "Jessie", "Stephen King");
		notice6 = new NoticeBibliographique("6", "Mercure", "Amélie Nothomb");
		notice7 = new NoticeBibliographique("7", "Frappe-toi le coeur", "Amélie Nothomb");
		notice8 = new NoticeBibliographique("8", "Survivant", "Chuck Palahniuk");
		listeNotices = new ArrayList<NoticeBibliographique>();
		retour = new ArrayList<NoticeBibliographique>();
		biblio = Bibliothèque.getInstance();
		biblio.addNotice(notice1);
		biblio.addNotice(notice3);
	}
	
	@Test
	void testConnexe5Titres() {
		//given
		listeNotices.add(notice1);
		listeNotices.add(notice2);
		listeNotices.add(notice3);
		listeNotices.add(notice4);
		listeNotices.add(notice5);
		retour.add(notice2);
		retour.add(notice3);
		retour.add(notice4);
		retour.add(notice5);
		when(glob.noticesDuMemeAuteurQue(notice1)).thenReturn(listeNotices);
		//when
		ArrayList<NoticeBibliographique> connexes = bibUtil.chercherNoticesConnexes(notice1);
		//then
		assertEquals(retour.size(),connexes.size());
	}
	
	@Test
	void testConnexe2Titres() {
		//given
		listeNotices.add(notice6);
		listeNotices.add(notice7);
		retour.add(notice7);
		when(glob.noticesDuMemeAuteurQue(notice6)).thenReturn(listeNotices);
		//when
		ArrayList<NoticeBibliographique> connexes = bibUtil.chercherNoticesConnexes(notice6);
		//then
		assertEquals(retour.size(),connexes.size());
	}
	
	@Test
	void testConnexe0Titres() {
		//given
		when(glob.noticesDuMemeAuteurQue(notice8)).thenReturn(listeNotices);
		//when
		ArrayList<NoticeBibliographique> connexes = bibUtil.chercherNoticesConnexes(notice8);
		//then
		assertEquals(retour.size(),connexes.size());
	}
	
	@Test
	void testAjoutNoticeInexistante() throws IncorrectIsbnException {
		//given
		listeNotices.add(notice8);
		when(glob.getNoticeFromIsbn("Minority Report")).thenThrow(IncorrectIsbnException.class);
		//when
		try {
			bibUtil.ajoutNotice("Minority Report");
			//then
			fail("On aurait du avoir une exception d'ajout impossible");
		} catch(AjoutImpossibleException e) {}
	}
	
	@Test
	void testAjoutNoticeCorrecteNew() throws IncorrectIsbnException, AjoutImpossibleException {
		//given
		when(glob.getNoticeFromIsbn("2")).thenReturn(notice2);
		//when
		NoticeStatus noticeStat = bibUtil.ajoutNotice("2");
		//then
		assertEquals(NoticeStatus.newlyAdded, noticeStat);
	}
	
	@Test
	void testAjoutNoticeCorrecteNoChange() throws IncorrectIsbnException, AjoutImpossibleException {
		//given
		when(glob.getNoticeFromIsbn("1")).thenReturn(notice1);
		//when
		NoticeStatus noticeStat = bibUtil.ajoutNotice("1");
		//then
		assertEquals(NoticeStatus.nochange,noticeStat);
	}
	
	@Test
	void testAjoutNoticeCorrecteUpdated() throws AjoutImpossibleException, IncorrectIsbnException {
		//given
		notice3 = new NoticeBibliographique("3", "La Peau sur les os", "Stephen King");
		when(glob.getNoticeFromIsbn("3")).thenReturn(notice3);
		//when
		NoticeStatus noticeStat = bibUtil.ajoutNotice("3");
		//then
		assertEquals(NoticeStatus.updated,noticeStat);
	}
	
	@Test
	void testPrévoirInventairePasBesoin() {
		//given
		LocalDate uneDate = LocalDate.of(2021, 05, 01);
		Clock fixedClock = Clock.fixed(uneDate.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
		//when
		boolean res = bibUtil.prévoirInventaire();
		//then
		assertFalse(res);
	}
	
	@Test
	void testPrévoirInventaireBesoin() {
		//given
		LocalDate autreDate = LocalDate.of(2022, 12, 01);
		Clock fixedClock = Clock.fixed(autreDate.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
		//when
		boolean res = bibUtil.prévoirInventaire();
		//then
		assertTrue(res);
	}
}
